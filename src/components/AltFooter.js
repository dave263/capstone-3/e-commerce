
import mern from './mern.png'

export default function AltFooter() {
	return(
		<div className="bg-dark fixed-bottom text-white d-flex justify-content-center align-items-center img-fluid" style={{height: '30vh'}}>
			<img src={mern} width="420" height="150"/>
		</div>
	)
}