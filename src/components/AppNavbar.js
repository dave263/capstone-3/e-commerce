import { Fragment, useContext, useEffect } from 'react'
import {Navbar, Container, Nav} from 'react-bootstrap'
import UserContext from './../UserContext'

import ecomlogo from './ecomlogo.png'

const token = localStorage.getItem('token')

export default function AppNavBar(){

	const { state, dispatch } = useContext(UserContext)
	console.log(state)

	const NavLinks = () => {

		if(state === true){
			return(
				<Fragment>
					<Nav.Link 
		        	href="/products" 
		        	className="text-light">Products</Nav.Link>
					<Nav.Link 
						href="/userdashboard" 
						className="text-light">Checkouts</Nav.Link>
					<Nav.Link 
						href="/logout" 
						className="text-light">Logout</Nav.Link>
				</Fragment>
			)
		} else {
			return(
				<Fragment>
					<Nav.Link 
		        	href="/" 
		        	className="text-light">Home</Nav.Link>
				    <Nav.Link 
				        href="/login" 
				        className="text-light">Login</Nav.Link>
				    <Nav.Link 
				        href="/register" 
				        className="text-light">Register</Nav.Link>
				</Fragment>
			)
		}
	}

	return(
		<Navbar bg="dark" expand="sm">
			<Container>
			<Navbar.Brand>
				<img
				src={ecomlogo}
				width="110"
				height="40"
				className="d-inline-block align-top"
				alt="amazing logo"
				/>
			</Navbar.Brand>
			</Container>
		  <Container>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      	<Nav className="d-flex ml-auto">
		        <NavLinks />	
		        </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}

