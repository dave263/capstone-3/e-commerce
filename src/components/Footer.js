

export default function Footer() {
	return(
		<div className="bg-dark text-white d-flex justify-content-center fixed-bottom" style={{height: '5vh', marginTop: '50px'}}>
			<p className="font-weight-bold"> &#169; 2022, Amazing E-commerce, Inc.</p>
		</div>

	)
}