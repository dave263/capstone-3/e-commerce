import '../style.css';
import {Card, Row, Col, Button} from "react-bootstrap";
import placeholderimg from './placeholder.png';


export default function HomeProducts({productProp}) {
	// console.log(productProp)

    const {productName, description} = productProp

    console.log(productProp)

        return(
            <Row>
            <Col sm={12}>
            <Card className="my-3 m-3">
            <Card.Img variant="top" src={placeholderimg} width="30"
				height="150"/>
              <Card.Body>
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>
                  {description}
                </Card.Text>
              </Card.Body>
            </Card>
            </Col>
            </Row>
        )

}