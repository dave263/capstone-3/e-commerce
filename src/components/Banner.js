import '../style.css';

export default function Banner({bannerProp}){
	// console.log(props)
	console.log(bannerProp)
	
	const {title, description} = bannerProp
	console.log(title)

	return(
		  <div id="banner">
		    <h1 className="display-3 text-center">{title}</h1>
		    <p className="lead text-center">{description}</p>
		  </div>
	)
}