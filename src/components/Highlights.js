import {Fragment} from "react";
import {Carousel, Row, Col, Button} from "react-bootstrap";

import one from "./1.png";
import two from "./2.png";
import three from "./3.png";

export default function Highlights(){
	return(

		<Fragment>
			<Row className="m-5">
				<Col>
					<Carousel>
					<Carousel.Item interval={1000}>
						<img
						className="d-block w-100"
						src={one}
						alt="First slide"
						/>
					</Carousel.Item>
					<Carousel.Item interval={500}>
						<img
						className="d-block w-100"
						src={two}
						alt="Second slide"
						/>
					</Carousel.Item>
					<Carousel.Item>
						<img
						className="d-block w-100"
						src={three}
						alt="Third slide"
						/>
					</Carousel.Item>
					</Carousel>
				</Col>
			</Row>
		</Fragment>
	)
}