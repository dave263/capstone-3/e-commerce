import {useState, useEffect} from 'react'
import {Card, Row, Col, Button} from "react-bootstrap"
import { Link } from 'react-router-dom'
import { useNavigate, useParams } from 'react-router-dom'
import placeholderimg from './placeholder.png'

import '../style.css'

export default function ProductCard({productProp}) {
	// console.log(productProp)

	const { productId } = useParams()
	const navigate = useNavigate()

	const {productName, description, price, _id} = productProp


	const handleBuy = (productId) => {

				navigate(`/products/${_id}`)
	
	}

	
	return(
		<Row>
		<Col sm={12}>
		<Card className="my-3 m-3">
		<Card.Img variant="top" src={placeholderimg} width="80"
				height="200"/>
		  <Card.Body>
		    <Card.Title>{productName}</Card.Title>
		    <Card.Subtitle>Description:</Card.Subtitle>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>
		    	{price}
		    </Card.Text>
			<Button
			className="btn btn-warning"
			onClick={ () => handleBuy(productId) }>Buy</Button>
		    {/* <Link to={`/products/${_id}`}> Check Product </Link> */}
		  </Card.Body>
		</Card>
		</Col>
        </Row>
	)
}