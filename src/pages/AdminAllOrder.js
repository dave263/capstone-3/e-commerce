import { useEffect, useState, Fragment, useContext } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from './../UserContext';

export default function AdminAllOrder() {

	const [allProducts, setAllProducts] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/all-orders`, {
			method: "POST",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllProducts( response.map(product => {
				

				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productId}</td>
						<td>{product.totalAmount}</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	return(
		<Fragment>
			<div className="body">
		<Container className="container">
			<h1 className="my-5 text-center">ALL ORDERS</h1>
            <div className="text-right">
				<Link className="btn btn-warning m-2" to={`/products`}>Products Dashboard</Link>
			</div>
			<Table>
				<thead>
					<tr>    
                        <th>Order ID</th>
						<th>Product ID</th>
						<th>Total Amount</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
		</Container>
		</div>
		</Fragment>
	)
}