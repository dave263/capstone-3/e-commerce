import { useState, useEffect, useContext, Fragment } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate, Link } from 'react-router-dom'

import UserContext from '../UserContext'

const token = localStorage.getItem('token')

export default function CreateProduct(){

	const [productName, setProductName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)

	const navigate = useNavigate()

	const { dispatch } = useContext(UserContext)


	const handleSubmit = (e) => {
		e.preventDefault()
		
		fetch('https://polar-falls-30691.herokuapp.com/api/products/create',{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)
			if(response){

				alert('Product successfully created!')

				navigate('/products')
			}
		})
		.catch(err => {
			console.log("Error Reading data " + err);
		  });

	}

	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}
	}, [])

	return(
		<div className="body2">
		<Container className="container mx-auto m-5">
		 	<h1 className="text-center">Add Product</h1>
			 <div className="text-center">
				 <Link className="btn btn-warning m-2" to={`/adminview`}>Back to Dashboard</Link>
				 </div>
			<Form onSubmit={ (e) => handleSubmit(e) }>
				<Row>
					<Col xs={12} md={8}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Name"
					    		type="text" 
					    		value={productName}
					    		onChange={ (e) => setProductName(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
					<Col xs={12}  md={4}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Price"
					    		type="number" 
					    		value={price}
					    		onChange={ (e) => setPrice(e.target.value) }
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Description"
					    		type="text" 
					    		value={description}
					    		onChange={ (e) => setDescription(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button type="submit" className="btn btn-warning btn-block">Add Product</Button>
			</Form>
		</Container>
		</div>
	)
}