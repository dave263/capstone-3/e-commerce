import { useState, useEffect, useContext, Fragment } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate, useParams, Link } from 'react-router-dom'

import UserContext from './../UserContext'

const token = localStorage.getItem('token')

export default function EditUser(){

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate()

    const { userId } = useParams()

	const { dispatch } = useContext(UserContext)


    useEffect(() => {
		if(firstName !== "" && lastName !== "" && email !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [firstName, lastName, email])


	const handleSubmit = (e) => {
		e.preventDefault()

		fetch(`https://polar-falls-30691.herokuapp.com/api/users/update-user`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: firstName,
                lastName: lastName,
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			if(response){

				alert('Your info has been updated!')

				navigate('/userdashboard')
			}
            }
        )
    }

    
	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}

	}, [])


    useEffect(() => {
		if(password !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [password])

    const handlePassword = (e) => {
		e.preventDefault()

		fetch(`https://polar-falls-30691.herokuapp.com/api/users/update-password`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				password: password
			})
		})
		.then(response => response.json())
		.then(response => {
			if(response){

				alert('Your password has been changed!')

				navigate('/products')
			} else {
                alert()
            }
            }
        )
    }

    
	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}

	}, [])

        return(
            <Fragment>
				<div className="body">
            <Container className="container m-5 mx-auto">
                 <h1 className="text-center">Change Your Information</h1>
				 <div className="text-center">
				 <Link className="btn btn-warning m-2" to={`/products`}>Back</Link>
				 </div>
                 <Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => handleSubmit(e) }>
						<Form.Group className="mb-3">
							<Form.Label>First Name</Form.Label>
					    	<Form.Control 
					    		type="text" 
					    		value={firstName}
					    		onChange={(e) => setFirstName(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Last Name</Form.Label>
					    	<Form.Control 
					    		type="text" 
					    		value={lastName}
					    		onChange={(e) => setLastName(e.target.value)}
					    	/>
						</Form.Group>

                        <Form.Group className="mb-3">
					    	<Form.Label>Email</Form.Label>
					    	<Form.Control 
					    		type="text" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>
						<Button 
							variant="warning" 
							type="submit"
							disabled={isDisabled}
						>
							Submit New User Info
						</Button>
					</Form>
				</Col>
			</Row>
            </Container>

                <Container className="m-5 mx-auto">
                 <h1 className="text-center">Change Your Password</h1>
                 <Row className="justify-content-center">
				    <Col xs={12} md={6}>
					<Form onSubmit={(e) => handlePassword(e) }>
						<Form.Group className="mb-3">
							<Form.Label>New Password</Form.Label>
					    	<Form.Control 
                                required
					    		type="password" 
					    		value={password}
					    		onChange={(e) => setPassword(e.target.value)}
					    	/>
						</Form.Group>
                        <Button 
							variant="warning" 
							type="submit"
							disabled={isDisabled}
						>
                            Submit New Password
                        </Button>
                        </Form>
                        </Col>
			</Row>
            </Container>
			</div>
            </Fragment>
        )        



}