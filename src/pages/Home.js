import {Fragment} from 'react';
import {useContext, useEffect, useState } from 'react';

import HomeProducts from './../components/HomeProducts';
import Banner from './../components/Banner';
import '../style.css'


export default function Home(){

	const data = {
		title: "Shop for anything you want",
		description: "We ship all over the Philippines!",
	}



	const [products, setProducts] = useState([])

	useEffect( () => {
			fetch(`https://polar-falls-30691.herokuapp.com/api/products/home-products`, {
				method: "GET",
				headers: {
					"Content-Type": "application/json"
				}
			})
			.then(response => response.json())
			.then(response => {
				console.log(response)


				setProducts(
					response.map(product => {
						// console.log(product)

						return <HomeProducts key={product._id} productProp={product}/>
					})
				)
			})
		

	}, [])

	
	return(
		<Fragment>
			<div id="homeproducts">
			<Banner bannerProp={data}/>
			<div className="container-fluid d-flex justify-content-around my-5 row">
				{products}
			</div>
			</div>
		</Fragment>
	)
}