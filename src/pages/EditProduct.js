import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate, useParams, Link } from 'react-router-dom'

import UserContext from './../UserContext'

const token = localStorage.getItem('token')

export default function EditProduct(){

	const [productName, setProductName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0)
    const [productIden, setProductIden] = useState('')
    const [productDesc, setProductDesc] = useState('')
	const [priceTag, setPriceTag] = useState(0)

	const navigate = useNavigate()

    const { productId } = useParams()


	const { dispatch } = useContext(UserContext)



	const fetchProducts = () => {
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			setProductIden(response.productName)
            setProductDesc(response.description)
            setPriceTag(response.price)
		})
	}

	useEffect(() => {
		if(token !== null){
			dispatch({type: "USER", payload: true})
		}

		fetchProducts()

	}, [])


	const handleSubmit = (e) => {
        e.preventDefault()
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}/update`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price
			})
		})
		.then(response => response.json())
		.then(response => {
                console.log(response)
                if(response){
                    navigate('/products')
                }
            }
        )
    }

    
	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}
	}, [])

        return(
            <div className="body2">
            <Container className="container m-5 mx-auto">
                 <h1 className="text-center">Update Product Info</h1>
                 <div className="text-center">
				 <Link className="btn btn-warning m-2" to={`/adminview`}>Back to Dashboard</Link>
				 </div>
                <Form onSubmit={ (e) => handleSubmit(e) }>
                    <Row>
                        <Col xs={12} md={8}>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    placeholder={productIden}
                                    type="text" 
                                    value={productName}
                                    onChange={ (e) => setProductName(e.target.value) }
                                    
                                />
                            </Form.Group>
                        </Col>
                        <Col xs={12}  md={4}>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    placeholder={priceTag}
                                    type="number" 
                                    value={price}
                                    onChange={ (e) => setPrice(e.target.value) }
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}  md={12}>
                            <Form.Group className="mb-3">
                                <Form.Control
                                    placeholder={productDesc}
                                    type="text" 
                                    value={description}
                                    onChange={ (e) => setDescription(e.target.value) }
                                    
                                />
                            </Form.Group>
                        </Col>
                    </Row>
                    <Button type="submit" className="btn btn-warning btn-block">Update</Button>
                </Form>
            </Container>
            </div>
        )       

}