import { useContext, useEffect, useState } from 'react'
import UserContext from './../UserContext'
import { Container, Row, Col, Card, Button } from 'react-bootstrap'
import { useParams, useNavigate } from 'react-router-dom'
import placeholderimg from '../components/placeholder.png'

const token = localStorage.getItem('token')
const userId = localStorage.getItem('userId')
// console.log(typeof token)
export default function SingleProduct(){

	const { dispatch } = useContext(UserContext)

	const { productId } = useParams()
	console.log(productId)

	const navigate = useNavigate()

	const [_id, setUserId] = useState("")
	const [productIden, setProductIden] = useState("")
	const [productName, setProductName] = useState("")
	const [price, setTotalAmount] = useState(0)



	const fetchProducts = () => {
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			setProductIden(response.productIden)
			setProductName(response.productName)
			setTotalAmount(response.price)
		})
	}

	useEffect(() => {
		if(token !== null){
			dispatch({type: "USER", payload: true})
		}

		fetchProducts()

	}, [])

	const handleBuy = (p) => {

		fetch(`https://polar-falls-30691.herokuapp.com/api/products/check-out`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId,
				productId: productId,
				productName: productName,
				totalAmount: price
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)
			if(response){

				alert('Product successfully checkedout!')

				navigate('/products')
			}
		})
	}

	return(
		<div className="body">
		<Container className="container">
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Card className="m-5">
					<Card.Img className="img-fluid" variant="top" src={placeholderimg} width="80"
					height="300"/>
					  <Card.Body>
					  <Card.Title className="m-2 text-danger">WARNING: THIS CAN ONLY BE CANCELLED WITHIN 1 HOUR AFTER CHECKOUT</Card.Title>	
					  	<Card.Subtitle>Your ID:</Card.Subtitle>
					    <Card.Text>{userId}</Card.Text>
					    <Card.Subtitle>Product ID:</Card.Subtitle>
					    <Card.Text>
					      {productId}
					    </Card.Text>
						<Card.Subtitle>Product Name:</Card.Subtitle>
					    <Card.Text>
					      {productName}
					    </Card.Text>
					    <Card.Subtitle>Amount:</Card.Subtitle>
					    <Card.Text>
					    	{price}
					    </Card.Text>
					    <Button 
					    	className="btn btn-warning"
					    	onClick={ () => handleBuy(productId) }
					    >Confirm Checkout</Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</div>
	)
}