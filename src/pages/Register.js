import {useState, useEffect} from 'react'
import {Form, Button, Row, Col, Container} from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'


export default function Register(){
	const [fN, setFN] = useState("")
	const [lN, setLN] = useState("")
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [vpw, setVPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	const navigate = useNavigate();

	useEffect(() => {

		if((fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") && (pw === vpw)){

			setIsDisabled(false)

		} else {
			setIsDisabled(true)
		}
	}, [fN, lN, email, pw, vpw])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('https://polar-falls-30691.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.text())
		.then(response => {
			console.log(response)
			if(response){
				fetch('https://polar-falls-30691.herokuapp.com/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw
				})
				})
				.then(response => response.text())
				.then(response => {
					console.log(response)
					if(response){
						alert('Registration Successful.')

						navigate('/login')
					} else
					{
						alert('Something went wrong. Please try again')
					}
				})
				.catch(err => {
					console.log("Error Reading data " + err);
				  });
			} else{
				alert(`User already exists`)
			}
		})
		.catch(err => {
			console.log("Error Reading data " + err);
		  });
	}


	return(
		<div className="loginreg my-4">
		<Container className="my-3 mx-auto">
		 	<h3 className="text-center">Register</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => registerUser(e) }>
						<Form.Group className="mb-3">
							<Form.Label>First Name</Form.Label>
					    	<Form.Control 
					    		type="text" 
					    		value={fN}
					    		onChange={(e) => setFN(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Label>Last Name</Form.Label>
					    	<Form.Control 
					    		type="text" 
					    		value={lN}
					    		onChange={(e) => setLN(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-1">
					    	<Form.Label>Verify Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={vpw}
					    		onChange={(e) => setVPW(e.target.value)}
					    	/>
						</Form.Group>
						<Button 
							variant="warning" 
							type="submit"
							disabled={isDisabled}
							className="my-3"
						>
							Submit
						</Button>
					</Form>
				</Col>
			</Row>
		</Container>
		</div>
	)
}