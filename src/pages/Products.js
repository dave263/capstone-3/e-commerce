import {Fragment, useContext, useEffect, useState } from 'react'
import AdminView from './AdminView'
import ProductCard from './../components/ProductCard'
import UserContext from './../UserContext'
import '../style.css'


const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')

export default function Products(){

	const { state, dispatch } = useContext(UserContext)
	console.log(state)

	const [products, setProducts] = useState([])

	useEffect( () => {
		if(admin === "false"){
			fetch(`https://polar-falls-30691.herokuapp.com/api/products/all-products`, {
				method: "GET",
				headers:{
					"Authorization": `Bearer ${localStorage.getItem('token')}`,
				}
			})
			.then(response => response.json())
			.then(response => {
				console.log(response)
				if(token !== null){
					dispatch({type: "USER", payload: true})
				}
				setProducts(
					response.map(product => {
						return <ProductCard key={product._id} productProp={product}/>
					})
				)
			})
		}

	}, [token])

	return(
		<Fragment>
			<div className="body mb-3">
			{
				admin === "false"
				?
					<div className="container-fluid d-flex justify-content-around my-5 row">
						{products}
					</div>
				:
						<AdminView/>
			}
			</div>
		</Fragment>
	)
}