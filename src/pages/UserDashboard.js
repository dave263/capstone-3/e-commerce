import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from './../UserContext'

export default function UserDashboard() {

	const [allProducts, setAllProducts] = useState([])
	const [_id, setUserId] = useState("")

	const { dispatch } = useContext(UserContext)

	const token = localStorage.getItem('token')
	const userId = localStorage.getItem('userId')


	const fetchData = () => {


		fetch(`
		https://polar-falls-30691.herokuapp.com/api/products/all-checkout`, {
			method: "POST",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userId: userId
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllProducts( 
				response.map(order => {
				

				return(
					<tr key={order._id}>
						<td>{order.productId}</td>
						<td>{order.productName}</td>
						<td>{order.totalAmount}</td>
                        <td>
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleCancel(order._id) }
									>
										Cancel Order
									</Button>
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {

		fetchData()

	}, [])

    const handleCancel = (orderId) =>{
		console.log(orderId)
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${orderId}/deleteco`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
			
				alert('You have cancelled the order')
			}
		})
	}

	return(
		<div className="body">
		<Container className="container">
			<h1 className="my-5 text-center">Your Checkouts</h1>
			<div className="text-right">
				<Link className="btn btn-warning m-2" to={`/update-user`}>Update User Info</Link>
			</div>
			<div className="text-right">
			</div>
			<Table>
				<thead>
					<tr>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Total Amount</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
		</Container>
		</div>
	)
}