import {Fragment, useState, useEffect, useContext} from 'react';
import {BrowserRouter} from "react-router-dom";
import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import UserContext from './../UserContext';
import AltFooter from '../components/AltFooter';

export default function Login(){
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)


	const { state, dispatch } = useContext(UserContext)
	const navigate = useNavigate()

	useEffect(() => {
		if(email !== "" && pw !== ""){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}
	}, [email, pw])

	const loginUser = (e) => {
		e.preventDefault()

		fetch('https://polar-falls-30691.herokuapp.com/api/users/login', {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				localStorage.setItem('token', response.token)
				localStorage.setItem('userId', response.token)
				const token = localStorage.getItem("token")

				fetch('https://polar-falls-30691.herokuapp.com/api/users/profile', {
				      method: "GET",
				      headers:{
				        "Authorization": `Bearer ${token}`
				      }
				    })
				    .then(response => response.json())
				    .then(response => {
						localStorage.setItem('userId', response._id)
				      localStorage.setItem('admin', response.isAdmin)

				      dispatch({type: "USER", payload: true})
				        
				    })
					.catch(err => {
						console.log("Error Reading data " + err);
					  });

				setEmail("")
				setPW("")

				 navigate('/products')
			} else {
				alert('Incorrect credentials!')
			}
		})
		.catch(err => {
			console.log("Error Reading data " + err);
		  });
	}


	return(
		<Fragment>
			<div className="loginreg">
		<Container className="m-5 mx-auto">
		 	<h3 className="text-center">Login</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => loginUser(e) }>
						<Form.Group className="mb-3">
							<Form.Label>Email address</Form.Label>
					    	<Form.Control 
					    		type="email" 
					    		value={email}
					    		onChange={(e) => setEmail(e.target.value)}
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Label>Password</Form.Label>
					    	<Form.Control 
					    		type="password" 
					    		value={pw}
					    		onChange={(e) => setPW(e.target.value)}
					    	/>
						</Form.Group>

						<Button 
							variant="warning" 
							type="submit"
							disabled={isDisabled}
						>
							SIGN IN
						</Button>
					</Form>
				</Col>
			</Row>
		</Container>
		<AltFooter/>
		</div>
		</Fragment>
	)
}