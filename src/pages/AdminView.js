import { useEffect, useState, Fragment, useContext } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from './../UserContext';

const token = localStorage.getItem('token')

export default function AdminView() {

	const [allProducts, setAllProducts] = useState([])

	const { dispatch } = useContext(UserContext)


	const fetchData = () => {
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/adminProducts`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllProducts( response.map(product => {
				

				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.productName}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								product.isActive ?
									<Fragment>
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleArchive(product._id) }
									>
										Archive
									</Button>
									<Link className="btn btn-info m-2" 
									to={`/${product._id}/update`}
									>
										Update
									</Link>
									</Fragment>
								:
									<Fragment>
										<Button 
											className="btn btn-warning mx-2"
											onClick={ () => handleUnarchive(product._id)}
										>
												Unarchive
										</Button>
										<Button 
											className="btn btn-secondary m-2"
											onClick={ () => handleDelete(product._id) }
										>
											Delete
										</Button>
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}

		fetchData()

	}, [])

	const handleArchive = (productId) =>{
		console.log(productId)
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}/archive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('Product is now archived!')
			}
		})
	}

	const handleUnarchive = (productId) =>{
		console.log(productId)
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}/unarchive`, {
			method: "PATCH",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Product successfully Unarchived!')
			}
		})
	}

	const handleDelete = (productId) =>{
		console.log(productId)
		fetch(`https://polar-falls-30691.herokuapp.com/api/products/${productId}/delete`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Product successfully deleted!')
			}
		})
	}

	return(
		<Fragment>
			<div className="body">
		<Container className="container">
			<h1 className="my-5 text-center">Product Dashboard</h1>
			<div className="text-right mx-auto">
				<Link className="btn btn-warning m-2" to={`/create`}>Add a product</Link>
				<Link className="btn btn-warning m-2" to={`/access`}>All Orders</Link>
				<Link className="btn btn-warning m-2" to={`/update-user`}>Update User Info</Link>
			</div>
			<div>
			<Table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Product Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
			</div>
		</Container>
		</div>
		</Fragment>
	)
}