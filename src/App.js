import { useReducer, useEffect  } from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { Container } from 'react-bootstrap'

import { UserProvider } from './UserContext'
import { initialState, reducer } from './reducer/UserReducer'
import './style.css'


import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import ErrorPage from './pages/ErrorPage';
import Logout from './pages/Logout';
import SingleProduct from './pages/SingleProduct';
import CreateProduct from './pages/CreateProduct';
import UserDashboard from './pages/UserDashboard';
import AdminView from './pages/AdminView';
import AdminAllOrder from './pages/AdminAllOrder';
import EditProduct from './pages/EditProduct';
import EditUser from './pages/EditUser';

import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

function App() {

  const [ state, dispatch ] = useReducer(reducer, initialState)
  console.log(state)

  return(
    <UserProvider value={{ state, dispatch }} >
      <BrowserRouter>
        <AppNavbar/>
        <Routes>
          <Route path="/" element={ <Home/> } />
          <Route path="*" element={ <ErrorPage/> } />
          <Route path="/register" element={ <Register/> } />
          <Route path="/login" element={ <Login/> } />
          <Route path="/logout" element={ <Logout/> } />
          <Route path="/products" element={ <Products/> } />
          <Route path="/products/:productId" element={ <SingleProduct/> } />
          <Route path="/update-user" element={ <EditUser /> } />
          <Route path="/create" element={ <CreateProduct /> } />
          <Route path="/:productId/update" element={ <EditProduct /> } />
          <Route path="/access" element={ <AdminAllOrder /> } />
          <Route path="/userdashboard" element={ <UserDashboard /> } />
          <Route path="/adminview" element={ <AdminView /> } />
        </Routes>
        <Footer/>
      </BrowserRouter>
    </UserProvider>
  )

}

export default App;
